# The Quorum Programming Language README #

This is the **Forked** version for **An Vo** and **Tony** attempting to enhance Sound Plugin

The Quorum programming language is a general purpose language designed for several purposes. First, we regularly run experiments with people at various age and experience ranges, investigating ways to make the language easier to use. Evidence gathered from these studies is filtered back into the design, making quorum an "evidence-based" programming language. Second, as our team is interested in issues of equity for all people, perhaps especially people with disabilities, we are careful to design libraries that are friendly to the broad population.

### Developing in Quorum ###

The easiest way to develop for Quorum is to use the corresponding development environment, Quorum Studio. Quorum Studio provides many of the basic features of Quorum, like compiling and running programs, out of the box. Quorum Studio can be downloaded from <insert link>

### Building Quorum from Source ###

The easiest way to build Quorum is in Quorum Studio. To build it, please follow these directions:
Visit the Bitbucket page to get the latest source code for Quorum: https://bitbucket.org/stefika/quorum-language
Clone the repository using Git from the terminal. The command will look something like "git clone https://stefika@bitbucket.org/stefika/quorum-language.git"
Open Quorum Studio
Inside of Quorum Studio, open Quorum, which is under quorum-language/Quorum
Build the project normally

###Log
03/05/2021
there are 2 options:
    1. make AudioSamples save an audio file and have it return a string so that
IOSSoundData can read
   2. Implement IOSSsoundData so that it can handle the short array of file
    3. convert audiosamples to File_
    4. ask where to locate the master project for netBeans
      5. ask to speak to alleew (from audiosamples) and Jeff Willson from FilePlugin
    

02/24/2021
What's "AudioSamples_"? And why are not using "AudioSamples" instead?

02/21/2021
AudioSamples class is at Plugin/SoundPlugin/SoundPlugin/src/plugins/quorum/Libraries/Sound
there are 17 related problems needed to be sorted out