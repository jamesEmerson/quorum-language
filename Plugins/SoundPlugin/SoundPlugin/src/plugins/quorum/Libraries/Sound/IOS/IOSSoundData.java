/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plugins.quorum.Libraries.Sound.IOS;


import java.util.Arrays;

import javax.sound.sampled.DataLine;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.AudioSystem;

import org.robovm.apple.foundation.NSArray;
import plugins.quorum.Libraries.Sound.AudioSamples;
import plugins.quorum.Libraries.Sound.Data;
import org.robovm.apple.foundation.Foundation;
import org.robovm.apple.foundation.NSString;
import org.robovm.apple.uikit.UIDevice;
import quorum.Libraries.Sound.*;

import sun.audio.*;

import java.nio.ByteBuffer;
import java.util.*;
import javax.sound.sampled.LineUnavailableException;

/**
 *
 * @author alleew
 */
public class IOSSoundData extends Data {
    private ALBuffer soundBuffer;
    private String soundPath;

    private ALChannelSource channel;
    private NSArray<ALSource> sourcePool;
    private StreamContainer streamIDs = new StreamContainer(8);

    private int index_audio_samples = 0;
    private StreamContainerAudioSamples streamSamples = new StreamContainerAudioSamples(8);

    private long soundID = -1;


    public IOSSoundData(quorum.Libraries.System.File_ quorumFile) {
        soundPath = quorumFile.GetAbsolutePath().replace('\\', '/');
        soundBuffer = OALSimpleAudio.sharedInstance().preloadEffect(soundPath);
        channel = OALSimpleAudio.sharedInstance().getChannelSource();
        sourcePool = channel.getSourcePool().getSources();
    }

    public IOSSoundData(quorum.Libraries.Sound.AudioSamples_ data) {
//        int channelInt = samples.GetChannels();
////      channel = (ALChannelSource)channelInt;
//        quorum.Libraries.System.File_ quorumFile = new quorum.Libraries.System.File(samples);
//        soundPath = quorumFile.GetAbsolutePath().replace('\\', '/');
//        soundBuffer = OALSimpleAudio.sharedInstance().preloadEffect((soundPath));
//        channel = OALSimpleAudio.sharedInstance().getChannelSource();
//        sourcePool = channel.getSourcePool().getSources();
        //channel = samples.GetChannels();
        soundPath = null;    // AudioSamples does not save data into a File; therefore, no sound path is needed
        soundBuffer = null;  // ALBuffer does not have a function to convert from one type to ALBuffer (because it
        // is a native class
        sourcePool = null;   // no NS array generated from audio Samples
        PlayAudioSamples(data);
    }

// *          Object me_
// *          int MAX_SHORT
// *          short[] buffer;
// *          int samplesPerSecond = 44100;
// *          int channels = 1;
//    public IOSSoundData(quorum.Libraries.Sound.AudioSamples_ samples)
//    OALSimpleAudio only works if an ALBuffer is passed
    
    /*
     * function void PlayAudioSamples is to play the audio Short buffer from AudioSamples
     * parameters: AudioSample_ samples
     * @return: non
     */
    public void PlayAudioSamples(quorum.Libraries.Sound.AudioSamples_ data) {
//        1. convert short[] to byte[]
//        2. read the byte array to the speakers
        AudioSamples samples = (AudioSamples) data;
                // initialize audio format
        int shortRead = 0;
        int bufferSize = 0;
        int i = 0;


        // convert shortBuffer to byteBuffer
        bufferSize = samples.GetTotalSize();
        
        SourceDataLine speakers = null;
        try{
            AudioFormat format = new AudioFormat(44100, 16, 1, true, true);           
            
            // testing integer 2 * 2 as buffer size
            DataLine.Info info = new DataLine.Info(SourceDataLine.class, format, bufferSize*2*2);
            
            if(!AudioSystem.isLineSupported(info))
            {
                throw new LineUnavailableException();
            }
            
            speakers = (SourceDataLine)AudioSystem.getLine(info);
            speakers.open(format);
            speakers.start();
            
        }catch (LineUnavailableException e) {
                    e.printStackTrace();
                    System.exit(-1);
        }

        // so short is larger than byte
        short[] short_buffer_array = samples.GetBufferShort();      
        ByteBuffer byte_buffer = ByteBuffer.allocate((bufferSize*2));  // return buffer size
        while (i < bufferSize) {
            byte_buffer.clear();
            byte_buffer.putShort(short_buffer_array[i]);
            
            i++;
        }

        byte[] byte_buffer_array = new byte[bufferSize * 2];
        byte_buffer_array = byte_buffer.array();
        // finished converting
        speakers.write(byte_buffer_array, 0, byte_buffer.position());
        
        speakers.drain();
        speakers.close();
        // read audio from the byte array using sun.audio package
//        AudioData audioData = new AudioData(byte_buffer_array);
//        AudioDataStream audioStream = new AudioDataStream(audioData);
//        AudioPlayer.player.start(audioStream);
    }

    @Override
    public void Play() {
        Play(isLooping);
    }

    public void Play(boolean loop) {
        if (streamIDs.size == 8)
            streamIDs.pop();
        ALSource soundSource = OALSimpleAudio.sharedInstance().playBuffer(soundBuffer, loop);
        if (soundSource == null)
            return;
        if (soundSource.getSourceId() == -1)
            return;
        streamIDs.insert(0, soundSource.getSourceId());
        soundID = soundSource.getSourceId();

        soundSource.setLooping(loop);
        soundSource.setVolume(volume);
        soundSource.setPitch(pitch);
        soundSource.setPosition(x, y, z);
    }

    @Override
    public void Stop() {
        ALSource source;
        for (int i = 0; i < streamIDs.size; i++) {
            if ((source = GetSoundSource(streamIDs.get(i))) != null)
                source.stop();
        }
    }

    @Override
    public void Dispose() {
        Stop();
        soundBuffer.dispose();
    }

    @Override
    public void Pause() {
        ALSource source;
        for (int i = 0; i < streamIDs.size; i++) {
            if ((source = GetSoundSource(streamIDs.get(i))) != null)
                source.setPaused(true);
        }
    }

    @Override
    public void Resume() {
        ALSource source;
        for (int i = 0; i < streamIDs.size; i++) {
            if ((source = GetSoundSource(streamIDs.get(i))) != null)
                source.setPaused(false);
        }
    }

    @Override
    public void SetPitch(float pitch) {
        this.pitch = pitch;
        ALSource source;
        if ((source = GetSoundSource(soundID)) != null)
            source.setPitch(pitch);
    }

    @Override
    public void SetVolume(float volume) {
        this.volume = volume;
        ALSource source;
        if ((source = GetSoundSource(soundID)) != null)
            source.setVolume(volume);
    }

    @Override
    public void SetReferenceDistance(float distance) {
        throw new UnsupportedOperationException("Reference distance controls haven't been implemented yet for iOS.");
    }

    @Override
    public void SetRolloff(float rolloff) {
        throw new UnsupportedOperationException("Reference distance controls haven't been implemented yet for iOS.");
    }

    @Override
    public void SetX(float newX) {
        SetPosition(newX, y, z);
    }

    @Override
    public void SetY(float newY) {
        SetPosition(x, newY, z);
    }

    @Override
    public void SetZ(float newZ) {
        SetPosition(x, y, newZ);
    }

    @Override
    public void SetPosition(float newX, float newY, float newZ) {
        x = newX;
        y = newY;
        z = newZ;

        ALSource source;
        if ((source = GetSoundSource(soundID)) != null)
            source.setPosition(x, y, z);
    }

    @Override
    public void EnableDoppler() {
        if (dopplerEnabled)
            return;

        dopplerEnabled = true;
        SetVelocity(velocityX, velocityY, velocityZ);
    }

    @Override
    public void DisableDoppler() {
        if (!dopplerEnabled)
            return;

        dopplerEnabled = false;

        ALSource source;
        if ((source = GetSoundSource(soundID)) != null)
            source.setVelocity(0, 0, 0);
    }

    @Override
    public void SetVelocity(float newX, float newY, float newZ) {
        velocityX = newX;
        velocityY = newY;
        velocityZ = newZ;

        ALSource source;
        if ((source = GetSoundSource(soundID)) != null)
            source.setVelocity(newX, newY, newZ);
    }

    @Override
    public void SetHorizontalPosition(float position) {
        this.pan = position;
        this.fade = 0;

        float newX = (float) Math.cos((pan - 1) * (float) Math.PI / 2);
        float newY = 0;
        float newZ = (float) Math.sin((pan + 1) * (float) Math.PI / 2);

        SetPosition(newX, newY, newZ);
    }

    @Override
    public void SetFade(float position) {
        this.pan = 0;
        this.fade = position;

        float newX = 0;
        float newY = (float) Math.cos((pan - 1) * (float) Math.PI / 2);
        float newZ = (float) Math.sin((pan + 1) * (float) Math.PI / 2);

        SetPosition(newX, newY, newZ);
    }

    @Override
    public void SetLooping(boolean looping) {
        this.isLooping = looping;
        ALSource source;
        if ((source = GetSoundSource(soundID)) != null)
            source.setLooping(looping);
    }

    @Override
    public boolean IsStreaming() {
        return false;
    }

    @Override
    public boolean IsPlaying() {
        ALSource source = GetSoundSource(soundID);
        if (source == null) {
            return false;
        }

        return source.isPlaying() && !source.isPaused();
    }

    @Override
    public void Update() {
        throw new RuntimeException("This audio was not set for streaming when loaded. Use LoadToStream to allow streaming the audio.");
    }

    @Override
    public void QueueSamples(AudioSamples_ samples) {
//        throw new RuntimeException("This audio was not set for streaming AudioSamples when loaded. Use LoadToStream(AudioSamples) to allow for sample queueing.");
//        streamSamples.insert(this.index_audio_samples, samples);
        this.index_audio_samples++;
    }

    @Override
    public void UnqueueSamples(AudioSamples_ samples) {
        throw new RuntimeException("This audio was not set for streaming AudioSamples when loaded. Use LoadToStream(AudioSamples) to allow for sample queueing.");
    }

    private ALSource GetSoundSource(long soundId) {

        for (ALSource source : sourcePool) {
            if (source.getSourceId() == soundId)
                return source;
        }
        return null;
    }


    /*
    A reduced implementation of the IntArray class from libGDX, using only the
    features needed for this class.
    */
    private class StreamContainer {
        public int[] items;
        public int size;
        public boolean ordered;

        /**
         * Creates an ordered array with a capacity of 16.
         */
        public StreamContainer() {
            this(true, 16);
        }

        /**
         * Creates an ordered array with the specified capacity.
         */
        public StreamContainer(int capacity) {
            this(true, capacity);
        }

        /**
         * @param ordered  If false, methods that remove elements may change the order of other elements in the array, which avoids a
         *                 memory copy.
         * @param capacity Any elements added beyond this will cause the backing array to be grown.
         */
        public StreamContainer(boolean ordered, int capacity) {
            this.ordered = ordered;
            items = new int[capacity];
        }

        public int get(int index) {
            if (index >= size) throw new IndexOutOfBoundsException("index can't be >= size: " + index + " >= " + size);
            return items[index];
        }

        public void insert(int index, int value) {
            if (index > size) throw new IndexOutOfBoundsException("index can't be > size: " + index + " > " + size);
            int[] items = this.items;
            if (size == items.length) items = resize(Math.max(8, (int) (size * 1.75f)));
            if (ordered)
                System.arraycopy(items, index, items, index + 1, size - index);
            else
                items[size] = items[index];
            size++;
            items[index] = value;
        }

        /**
         * Removes and returns the last item.
         */
        public int pop() {
            return items[--size];
        }

        public int[] resize(int newSize) {
            int[] newItems = new int[newSize];
            int[] items = this.items;
            System.arraycopy(items, 0, newItems, 0, Math.min(size, newItems.length));
            this.items = newItems;
            return newItems;
        }
    }
    
    

    /*
     * class StreamContainerAudioSamples is designed to be a stream container for
     * AudioSamples
     */
    private class StreamContainerAudioSamples {
        public int size;
        public boolean ordered;
        public AudioSamples[] items;

        /**
         * Creates an ordered array with a capacity of 16.
         */
        public StreamContainerAudioSamples() {
            this(true, 16);
        }

        public StreamContainerAudioSamples(boolean b, int i) {
            this.ordered = b;
            items = new AudioSamples[i];
        }

        public StreamContainerAudioSamples(int i) {
            this.ordered = false;
            items = new AudioSamples[i];
        }
        

    // re-implement reszie for insert function
        
        public void insert (int index, AudioSamples_ audioSamples) {
            AudioSamples audioSamplesCast = (AudioSamples) audioSamples;
            if (index > size) throw new IndexOutOfBoundsException("index can't be > size: " + index + " > " + size);
            AudioSamples[] items = this.items;
            
            if (size == items.length) 
                items = resize(Math.max(8, (int)(size * 1.75f)));
            
            if (ordered)
                System.arraycopy(items, index, items, index + 1, size - index);
            else
                items[size] = items[index];
            size++;
            items[index] = audioSamplesCast;
        }

        private AudioSamples[] resize(int max) {
            int newSize = 0;
            AudioSamples[] newItems = new AudioSamples[newSize];
            AudioSamples[] items = this.items;
            System.arraycopy(items, 0, newItems, 0, Math.min(size, newItems.length));
            //this.items = newItems;
            return newItems;
        }
        
        
    }
    
    @Override
    public int GetSampleOffset()
    {
        return 0;
    }
}
